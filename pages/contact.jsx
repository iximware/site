import Layout from "@/src/layout/Layout";
const Contacts = () => {
  return (
    <Layout noHeaderBg pageName={"Contact"}>
      <section
        className="splash-area-section"
        style={{ backgroundImage: "url(assets/img/background.jpg)" }}
      >
        <div className="container">
          <div className="splash-area">
            <h2>Let's talk about you need</h2>
            <a href="#">Connect with Us!</a>
          </div>
        </div>
      </section>
      <section className="contact-page gap">
        <div className="container">
          <div className="heading">
            <h6>Kickstart Your Project with Our Team.</h6>
            <h2>Drop Us a Line</h2>
          </div>
          <div className="row">
            <div className="col-xl-6 col-lg-6">
              <form className="content-form" action="https://api.web3forms.com/submit" method="POST">
                <div className="row">
                  <div className="col-lg-6">
                    <input type="text" name="name" placeholder="Your Name *" required/>
                  </div>
                  <div className="col-lg-6">
                    <input
                      type="text"
                      name="company"
                      placeholder="Your Company *"
                      required
                    />
                  </div>
                </div>
                <input type="text" name="email" placeholder="Your Email *" required/>
                <textarea name="message" placeholder="Your Message *" defaultValue={""} required />
                <input type="hidden" name="access_key" value="c0f22bbb-369f-4669-a995-5e3a3f012c40"></input>
                <button className="themebtu" type="submit">Submit</button>
              </form>
            </div>
            <div className="offset-lg-1 col-xl-5 col-lg-5">
              <ul className="sidebar">
                <li>
                  <h4>Address : </h4>
                  <span>
                    Juarez 179, Ixtapa Centro
                    <br />
                    Puerto Vallarta, Jal 48280
                  </span>
                </li>
                <li>
                  <h4>Phone :</h4>
                  <a href="callto:528132707667">
                    <span>+52 813 270 7667</span>
                  </a>
                </li>
                <li>
                  <h4>Email :</h4>
                  <a href="mailto:contact@iximware.mx">
                    <span>contact@iximware.mx</span>
                  </a>
                </li>
                <li>
                  <h4>Connect with us :</h4>
                  <ul className="brandicon">
                    <li>
                      <a href="https://www.facebook.com/iximwaremx">
                        <i className="fa-brands fa-facebook-f" />
                      </a>
                    </li>
                    <li>
                      <a href="https://x.com/iximware">
                        <i className="fa-brands fa-twitter" />
                      </a>
                    </li>
                    <li>
                      <a href="https://www.instagram.com/iximware/">
                        <i className="fa-brands fa-instagram" />
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <div className="map">
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d318.2524357339499!2d-105.20704165576184!3d20.7143080953639!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842148bcbe11c565%3A0x938917e568bccb91!2sJu%C3%A1rez%20179%2C%20Centro%20Ixtapa%2C%2048280%20Ixtapa%2C%20Jal.!5e0!3m2!1sen!2smx!4v1711776926798!5m2!1sen!2smx"
          style={{ border: 0 }}
          allowFullScreen
          loading="lazy"
          referrerPolicy="no-referrer-when-downgrade"
        />
      </div>
      <div className="container">
        <div className="awesome-project">
          <h4>Let's Start an Awesome Project Now!</h4>
          <a href="contacts.html" className="themebtu full">
            Get Started
          </a>
        </div>
      </div>
    </Layout>
  );
};
export default Contacts;

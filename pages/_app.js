import Preloader from '@/src/layout/Preloader';
import '@/styles/globals.css';
import { Fragment, useEffect, useState } from 'react';
const App = ({ Component, pageProps }) => {
    const [loader, setLoader] = useState(true);
    useEffect(() => {
        setTimeout(() => {
            setLoader(false);
        }, 1000);
    }, []);

    return (
        <Fragment>
            <Preloader />
            {!loader && <Component {...pageProps} />}
        </Fragment>
    );
};
export default App;

import Layout from "@/src/layout/Layout";

const TallerAdministracionProyectos = () => {
  return (
    <Layout headerExtraClass={"three"} pageName={"Taller de adminstración de proyectos"} pageDescription={"Taller de Administración de Proyectos en Puerto Vallarta: aprende planificación, gestión de recursos y herramientas prácticas."}>
      <section>
        <div className="container">
          <div className="title-blog">
            <h2>Taller de aministración de proyectos</h2>
          </div>
          <div className="row">
            <div className="col-xl-8">
              <div className="blog-item">
                <img alt="img" src="assets/img/workshop-pm.jpg" />
                <h6 className="pt-5">
                  Descubre las Ventajas del Taller de Administración de Proyectos para Impulsar tu Negocio
                </h6>
                <p className="p-30">
                  Si lideras proyectos y manejas equipos o recursos dentro de tu organización, sabrás lo importante que es gestionar bien el tiempo, las tareas y las prioridades para que los proyectos avancen y se obtengan los resultados esperados. Sin una buena planificación, los retrasos y los imprevistos pueden volverse algo cotidiano. Por eso, hemos creado un **taller de Administración de Proyectos** pensado específicamente para personas como tú, que buscan ser más organizadas y eficientes.
									<br/>
									Aquí te explicamos algunas de las **ventajas** de asistir a este taller el <strong>19 de octubre en Puerto Vallarta</strong>:
                </p>
                <div className="my-5">
									<figure class="text-center">
									<blockquote class="blockquote">
										<p>Las grandes cosas no se hacen por impulso, sino por una serie de pequeños esfuerzos.</p>
									</blockquote>
									<figcaption class="blockquote-footer">
										<cite title="Source Title">Vincent Van Gogh</cite>
									</figcaption>
								</figure>
								</div>
                <h6 className="py-3">Planificación Clara desde el Primer Día</h6>
                <p>Manejar proyectos de manera eficiente es clave para el éxito de cualquier equipo. En este taller aprenderás a <strong>planificar cada etapa*</strong> del proyecto, de forma que puedas ver el panorama completo y anticiparte a cualquier obstáculo. Esto no solo mejora el flujo de trabajo, sino que también reduce el estrés que viene con la falta de organización.</p>
								<h6 className="py-3">Aprovechamiento Óptimo del Tiempo y los Recursos</h6>
								<p>¿Sientes que nunca hay suficiente tiempo en el día? Te enseñaremos cómo <strong>gestionar mejor tu tiempo</strong> y los recursos disponibles para que tus proyectos no se extiendan más de lo necesario. Con las herramientas adecuadas, aprenderás a optimizar cada minuto y a hacer más con lo que ya tienes, un aspecto crucial para cualquier empresa en crecimiento.</p>
								<h6 className="py-3">Herramientas Fáciles y Prácticas</h6>
								<p>No necesitas ser un experto en tecnología para administrar bien un proyecto. Durante el curso, exploraremos <strong>herramientas simples pero efectivas</strong> que te ayudarán a monitorear el progreso de tus tareas, delegar funciones y mantenerte al tanto de todo, sin complicaciones. Estas herramientas son accesibles para cualquier tipo de negocio y serán de gran ayuda en tu día a día.</p>
								<h6 className="py-3">Flexibilidad para Cualquier Sector</h6>
								<p>Ya sea que estés en el comercio, servicios o cualquier otro sector, lo que aprendas en este taller será <strong>fácilmente aplicable</strong>. Nos enfocamos en ofrecerte métodos que se puedan ajustar a cualquier tipo de proyecto, ya sea grande o pequeño, para que tengas la confianza de llevar tus ideas a buen puerto, sin importar el tamaño de tu negocio.</p>
								<h6>Conexiones que Impulsan tu Negocio</h6>
								<p>Al asistir a este taller, no solo adquirirás nuevas habilidades, sino que también tendrás la oportunidad de <strong>hacer networking</strong> con otros profesionales que enfrentan los mismos desafíos que tú. Es una excelente oportunidad para aprender de las experiencias de otros y ampliar tu red de contactos.</p>

								<p className="py-3"><strong>El taller tiene un cupo limitado a solo 20 personas, así que asegúrate de reservar tu lugar lo antes posible.</strong></p>
								
								<p>No pierdas la oportunidad de mejorar tu organización y hacer crecer tu negocio de manera efectiva. <strong>Envía la palabra "RESERVAR" al WhatsApp</strong> 📲 +52 8132707667 y prepárate para llevar tus proyectos al siguiente nivel.</p>

								<p className="my-4">¡Te esperamos en Puerto Vallarta!</p>
							
							</div>
              <div className="map">
                <h6>¿Dónde será?</h6>
								<p>En el interior del Centro Internacional de convenciones de Puerto Vallarta</p>
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3732.7449627511614!2d-105.23608008836892!3d20.67995239958626!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8421450044b44b89%3A0x3d6f2dac67280a5b!2sREDi%20Puerto%20Vallarta!5e0!3m2!1ses-419!2smx!4v1728495183198!5m2!1ses-419!2smx"
                            style={{ border: 0 }}
                            allowFullScreen
                            loading="lazy"
                            referrerPolicy="no-referrer-when-downgrade"
                        />
                    </div>
            </div>
            <div className="col-xl-4 pl-60">
              <ul className="sidebar">
                <li>
                  <a href="https://wa.me/528132707667" className="themebtu">
                    ¡Reserva ahora!
                  </a>
                </li>
                <li>
                  <h4>Hora:</h4>
                  <span>10:00 a.m. - 1:30 p.m.</span>
                </li>
                <li>
                  <h4>Fecha:</h4>
                  <span>19 Octubre 2024</span>
                </li>
                <li>
                  <h4>Ubicación:</h4>
                  <a href="https://maps.app.goo.gl/WUyimhAoK6YHpoFm8">Interior del Centro Internacional de Convenciones Puerto Vallarta</a>
                </li>
                <li>
                  <h4>Comparte la información:</h4>
                  <ul className="brandicon">
                    <li>
                      <a href="https://www.facebook.com/sharer/sharer.php?u=https://iximware.mx/taller-administracion-proyectos">
                        <i className="fa-brands fa-facebook-f" />
                      </a>
                    </li>
                    <li>
                      <a href="https://twitter.com/intent/tweet?url=http://iximware.mx/taller-administracion-proyectos&text=Taller Administracion de Proyectos">
                        <i className="fa-brands fa-twitter" />
                      </a>
                    </li>
                    <li>
                      <a href="https://www.linkedin.com/sharing/share-offsite/?url=http://iximware.mx/taller-administracion-proyectos">
                        <i className="fa-brands fa-linkedin-in" />
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <section className="gap blog-recent-posts">
        <div className="container">
          <h4 className="mb-5">Other events</h4>
          <div className="row">
            <div className="col-xl-4 col-md-6">
                <div className="latest-blog-post hoverstyle">
                  <figure>
                    <img
                      alt="img"
                      className="w-100"
                      src="assets/img/fuckup-nite-tb.jpg"
                    />
                  </figure>
                  <a href="#">
                    <i className="fa-regular fa-clock" />
                    <span>April, 2024</span>
                  </a>
                  <a href="fuckup-nite">
                    <h4>Fuckup nite!</h4>
                  </a>
                  <span>by iximware</span>
                </div>
              </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};
export default TallerAdministracionProyectos;

import Layout from "@/src/layout/Layout";

const FuckupNite = () => {
  return (
    <Layout headerExtraClass={"three"} pageName={"F*ckup Nite!"} pageDescription={"Discover the Power of Failure: Join Us at F*ckup Nite!"}>
      <section>
        <div className="container">
          <div className="title-blog">
            <h2>Fuckup Nite!</h2>
          </div>
          <div className="row">
            <div className="col-xl-8">
              <div className="blog-item">
                <img alt="img" src="assets/img/fuckup-nite.jpg" />
                <h6 className="pt-5">
                  Discover the Power of Failure: Join Us at Fuckup Nite!
                </h6>
                <p className="p-30">
                  Failure. It's a word that often evokes fear, discomfort, and even embarrassment. Yet, what if we told you that failure holds the key to some of life's greatest lessons and successes?
                  <br/><br/>
                  Welcome to Fuckup Nite – an event like no other, where failure takes center stage, celebrated rather than shunned. We invite you to join us on a journey of enlightenment, inspiration, and growth as we embrace the beauty of failure and the resilience it breeds.
                  <br/><br/>
                  At Fuckup Nite, we believe that behind every success story lies a trail of failures, setbacks, and wrong turns. It's these moments of vulnerability and learning that shape us, mold us, and ultimately propel us forward on our paths to greatness.
                </p>
                <div className="quote-bg">
                    Embrace the power of failure, celebrate the beauty of imperfection, and join us at Fuckup Nite – where failure isn't the end of the story, but the beginning of something extraordinary.
                </div>
                <h6>So, why attend Fuckup Nite? Here are just a few reasons:</h6>
                <ol>
                  <li>Real Stories, Real Impact: Prepare to be captivated by raw, unfiltered tales of failure from individuals who have dared to dream big, stumble hard, and rise stronger. From entrepreneurial ventures gone awry to personal missteps and professional blunders, our speakers lay bare their failures for all to see – and the lessons they've gleaned along the way.</li>
                  <li>Empowerment Through Vulnerability: In a world that often glorifies success while sweeping failure under the rug, Fuckup Nite offers a refreshing dose of authenticity and vulnerability. Here, imperfection is not only accepted but celebrated as a badge of honor, fostering a culture of openness, honesty, and mutual support.</li>
                  <li>Learn, Grow, Repeat: Failure isn't the end – it's just the beginning. At Fuckup Nite, we believe that every setback is an opportunity for growth, learning, and evolution. Whether you're an aspiring entrepreneur, seasoned professional, or curious soul seeking inspiration, you'll walk away from our event with valuable insights, perspectives, and tools to navigate failure and forge your own path to success.</li>
                  <li>Connect and Collaborate: Join a community of like-minded individuals who aren't afraid to fail boldly, dream audaciously, and support one another along the way. Fuckup Nite isn't just an event – it's a movement, a tribe, a catalyst for change. Forge meaningful connections, share your stories, and embark on a journey of self-discovery and transformation alongside fellow travelers on the road less traveled.</li>
                </ol>
              </div>
              <div className="map">
                <h6>Location</h6>
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d318.2524357339499!2d-105.20704165576184!3d20.7143080953639!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842148bcbe11c565%3A0x938917e568bccb91!2sJu%C3%A1rez%20179%2C%20Centro%20Ixtapa%2C%2048280%20Ixtapa%2C%20Jal.!5e0!3m2!1sen!2smx!4v1711776926798!5m2!1sen!2smx"
                            style={{ border: 0 }}
                            allowFullScreen
                            loading="lazy"
                            referrerPolicy="no-referrer-when-downgrade"
                        />
                    </div>
            </div>
            <div className="col-xl-4 pl-60">
              <ul className="sidebar">
                <li>
                  <a href="https://www.eventbrite.com/e/fckup-nite-tickets-875399241697?utm-campaign=social&utm-content=attendeeshare&utm-medium=discovery&utm-term=listing&utm-source=cp&aff=ebdsshcopyurl" className="themebtu">
                    Book now!
                  </a>
                </li>
                <li>
                  <h4>Tickets:</h4>
                  <a href="https://www.eventbrite.com/e/fckup-nite-tickets-875399241697?utm-campaign=social&utm-content=attendeeshare&utm-medium=discovery&utm-term=listing&utm-source=cp&aff=ebdsshcopyurl">
                    Eventbrite
                  </a> 
                </li>
                <li>
                  <h4>Time:</h4>
                  <span>18:00</span>
                </li>
                <li>
                  <h4>Date:</h4>
                  <span>April 25th, 2024</span>
                </li>
                <li>
                  <h4>Location:</h4>
                  <span>Juarez 179, Ixtapa Centro</span>
                </li>
                <li>
                  <h4>Share post :</h4>
                  <ul className="brandicon">
                    <li>
                      <a href="#">
                        <i className="fa-brands fa-facebook-f" />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="fa-brands fa-twitter" />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="fa-brands fa-instagram" />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="fa-brands fa-linkedin-in" />
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <section className="gap blog-recent-posts">
        <div className="container">
          <h4 className="mb-5">Other events</h4>
          <div className="row">
            <div className="col-xl-4 col-md-6">
                <div className="latest-blog-post hoverstyle">
                  <figure>
                    <img
                      alt="img"
                      className="w-100"
                      src="assets/img/fuckup-nite-tb.jpg"
                    />
                  </figure>
                  <a href="#">
                    <i className="fa-regular fa-clock" />
                    <span>April, 2024</span>
                  </a>
                  <a href="fuckup-nite">
                    <h4>Fuckup nite!</h4>
                  </a>
                  <span>by iximware</span>
                </div>
              </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};
export default FuckupNite;

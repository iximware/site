import Layout from "@/src/layout/Layout";

const WorkshopPM = () => {
  return (
    <Layout headerExtraClass={"three"} pageName={"Workshop Project Management"} pageDescription={"Taller de Administración de Proyectos en Puerto Vallarta: aprende planificación, gestión de recursos y herramientas prácticas."}>
      <section>
        <div className="container">
          <div className="title-blog">
            <h2>Workshop Project Management</h2>
          </div>
          <div className="row">
            <div className="col-xl-8">
              <div className="blog-item">
                <img alt="img" src="assets/img/workshop-pm.jpg" />
                <h6 className="pt-5">
                  Discover the Benefits of the Project Management Workshop to Boost Your Business
                </h6>
                <p className="p-30">
                  If you lead projects and manage teams or resources within your organization, you know how important it is to manage time, tasks, and priorities effectively to keep projects moving forward and achieve the expected results. Without proper planning, delays and unexpected issues can become the norm. That’s why we’ve created a <strong>Project Management Workshop</strong> specifically designed for people like you, who want to be more organized and efficient.
									<br/>
									Here are some of the <strong>key benefits</strong> of attending this workshop on <strong>October 19th in Puerto Vallarta</strong>:
                </p>
                <div className="my-5">
									<figure class="text-center">
									<blockquote class="blockquote">
										<p>Great things are not done by impulse, but by a series of small efforts.</p>
									</blockquote>
									<figcaption class="blockquote-footer">
										<cite title="Source Title">Vincent Van Gogh</cite>
									</figcaption>
								</figure>
								</div>
                <h6 className="py-3">Clear Planning from Day One</h6>
                <p>Managing projects efficiently is key to the success of any team. In this workshop, you'll learn how to <strong>plan each phase</strong> of the project, giving you a complete view and allowing you to anticipate obstacles. This not only improves workflow but also reduces the stress that comes with poor organization.</p>
								<h6 className="py-3">Optimal Use of Time and Resources</h6>
								<p>Do you feel like there’s never enough time in the day? We'll teach you how to <strong>better manage your time</strong> and available resources, so your projects don’t drag on longer than necessary. With the right tools, you’ll learn to optimize every minute and do more with what you already have, a crucial skill for any growing business.</p>
								<h6 className="py-3">Simple and Practical Tools</h6>
								<p>You don’t need to be a tech expert to manage a project well. In the course, we’ll explore <strong>simple yet effective tools</strong> that will help you track task progress, delegate functions, and stay on top of everything without the hassle. These tools are accessible for any type of business and will greatly benefit your day-to-day operations.</p>
								<h6 className="py-3">Flexibility for Any Industry</h6>
								<p>Whether you're in retail, services, or any other sector, what you’ll learn in this workshop will be <strong>easily applicable</strong>. We focus on providing you with methods that can be adjusted to any type of project, large or small, so you’ll have the confidence to bring your ideas to fruition, regardless of your business size.</p>
								<h6>Connections that Boost Your Business</h6>
								<p>In addition to gaining new skills, this workshop gives you the chance to <strong>network with other professionals</strong> facing the same challenges as you. It’s an excellent opportunity to learn from others’ experiences and expand your network of contacts.</p>

								<p className="py-3"><strong>The workshop is limited to only 20 participants, so make sure to reserve your spot as soon as possible.</strong></p>
								
								<p>Don’t miss the opportunity to improve your organization and grow your business effectively. <strong>Send the word “RESERVE” to WhatsApp 📲 +52 81 3270 7667</strong> and get ready to take your projects to the next level.</p>

								<p className="my-4">See you in Puerto Vallarta!</p>
							
							</div>
              <div className="map">
                <h6>Location</h6>
								<p>Inside of Centro Internacional de convenciones de Puerto Vallarta</p>
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3732.7449627511614!2d-105.23608008836892!3d20.67995239958626!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8421450044b44b89%3A0x3d6f2dac67280a5b!2sREDi%20Puerto%20Vallarta!5e0!3m2!1ses-419!2smx!4v1728495183198!5m2!1ses-419!2smx"
                            style={{ border: 0 }}
                            allowFullScreen
                            loading="lazy"
                            referrerPolicy="no-referrer-when-downgrade"
                        />
                    </div>
            </div>
            <div className="col-xl-4 pl-60">
              <ul className="sidebar">
                <li>
                  <a href="https://wa.me/528132707667" className="themebtu">
                    ¡Book now!
                  </a>
                </li>
                <li>
                  <h4>Time:</h4>
                  <span>10:00 a.m. - 1:30 p.m.</span>
                </li>
                <li>
                  <h4>Date:</h4>
                  <span>19 Octubre 2024</span>
                </li>
                <li>
                  <h4>Location:</h4>
                  <a href="https://maps.app.goo.gl/WUyimhAoK6YHpoFm8">Interior del Centro Internacional de Convenciones Puerto Vallarta</a>
                </li>
                <li>
                  <h4>Spread the info:</h4>
                  <ul className="brandicon">
                    <li>
                      <a href="https://www.facebook.com/sharer/sharer.php?u=https://iximware.mx/taller-administracion-proyectos">
                        <i className="fa-brands fa-facebook-f" />
                      </a>
                    </li>
                    <li>
                      <a href="https://twitter.com/intent/tweet?url=http://iximware.mx/taller-administracion-proyectos&text=Taller Administracion de Proyectos">
                        <i className="fa-brands fa-twitter" />
                      </a>
                    </li>
                    <li>
                      <a href="https://www.linkedin.com/sharing/share-offsite/?url=http://iximware.mx/taller-administracion-proyectos">
                        <i className="fa-brands fa-linkedin-in" />
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <section className="gap blog-recent-posts">
        <div className="container">
          <h4 className="mb-5">Other events</h4>
          <div className="row">
            <div className="col-xl-4 col-md-6">
                <div className="latest-blog-post hoverstyle">
                  <figure>
                    <img
                      alt="img"
                      className="w-100"
                      src="assets/img/fuckup-nite-tb.jpg"
                    />
                  </figure>
                  <a href="#">
                    <i className="fa-regular fa-clock" />
                    <span>April, 2024</span>
                  </a>
                  <a href="fuckup-nite">
                    <h4>Fuckup nite!</h4>
                  </a>
                  <span>by iximware</span>
                </div>
              </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};
export default WorkshopPM;

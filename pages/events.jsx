import ContactForm from "@/src/components/ContactForm";
import Layout from "@/src/layout/Layout";
const Events = () => {
  return (
    <Layout headerExtraClass={"three"} pageName={"Events"}>
      <div>
        <section className="gap no-top">
          <div className="container">
            <div className="splash-area blog">
              <h2>
                Latest News
                <br />
                From Our Events
              </h2>
            </div>
            <div className="row">
              <div className="col-xl-4 col-md-6">
                <div className="latest-blog-post hoverstyle">
                  <figure>
                    <img
                      alt="img"
                      className="w-100"
                      src="assets/img/fuckup-nite-tb.jpg"
                    />
                  </figure>
                  <a href="#">
                    <i className="fa-regular fa-clock" />
                    <span>April, 2024</span>
                  </a>
                  <a href="fuckup-nite">
                    <h4>Fuckup nite!</h4>
                  </a>
                  <span>by iximware</span>
                </div>
              </div>
            </div>
          </div>
        </section>
				<section className="gap map-three">
					<ContactForm/>
				</section>
      </div>
    </Layout>
  );
};
export default Events;

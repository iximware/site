import ContactForm from "@/src/components/ContactForm";
import Layout from "@/src/layout/Layout";
const PM = () => {
  return (
    <Layout pageName={"Project management"}>
      <section className="hero-section-three">
        <div
          className="swiper-container herothreeswiper"
        >
          <div className="swiper-wrapper">
              <div className="row align-items-center">
                <div className="col-xl-4 offset-xl-1">
                  <div className="heading-boder">
                    <h2>
                      The right methodology make the difference!
                    </h2>
                    <p>
                      Analyze, Measure and Organize your projects to success.
                    </p>
                  </div>
                </div>
                <div className="col-xl-7">
                  <div className="three-page-img">
                    <img
                      alt="img"
                      className="w-100"
                      src="assets/img/project-management-hero.jpg"
                    />
                  </div>
                </div>
              </div>
          </div>
          <div className="swiper-pagination two-style" />
        </div>
      </section>
      <section
        className="makes-us-different"
        style={{ backgroundColor: "#f2edf5" }}
      >
        <div className="container">
          <div className="heading">
            <h6>How It Works?</h6>
            <h2>What Makes Us the option?</h2>
          </div>
          <div className="row">
            <div className="col-xl-4 col-lg-6 ">
              <div className="makes-us-different-text">
                <i>
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M6 3.5A1.5 1.5 0 0 1 7.5 2h1A1.5 1.5 0 0 1 10 3.5v1A1.5 1.5 0 0 1 8.5 6v1H14a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-1 0V8h-5v.5a.5.5 0 0 1-1 0V8h-5v.5a.5.5 0 0 1-1 0v-1A.5.5 0 0 1 2 7h5.5V6A1.5 1.5 0 0 1 6 4.5zM8.5 5a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5zM0 11.5A1.5 1.5 0 0 1 1.5 10h1A1.5 1.5 0 0 1 4 11.5v1A1.5 1.5 0 0 1 2.5 14h-1A1.5 1.5 0 0 1 0 12.5zm1.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5zm4.5.5A1.5 1.5 0 0 1 7.5 10h1a1.5 1.5 0 0 1 1.5 1.5v1A1.5 1.5 0 0 1 8.5 14h-1A1.5 1.5 0 0 1 6 12.5zm1.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5zm4.5.5a1.5 1.5 0 0 1 1.5-1.5h1a1.5 1.5 0 0 1 1.5 1.5v1a1.5 1.5 0 0 1-1.5 1.5h-1a1.5 1.5 0 0 1-1.5-1.5zm1.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5z"/>
</svg>
                </i>
                <h4>Functional solutions</h4>
                <p>
                  Creative solutions for your business needs focused on client's experience.
                </p>
              </div>
            </div>
            <div className="col-xl-4 col-lg-6 ">
              <div className="makes-us-different-text">
                <i>
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
  <path d="M5 8.5A2.5 2.5 0 0 1 7.5 6H9V4.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L9.41 8.658A.25.25 0 0 1 9 8.466V7H7.5A1.5 1.5 0 0 0 6 8.5V11H5z"/>
  <path fill-rule="evenodd" d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.48 1.48 0 0 1 0-2.098zm1.4.7a.495.495 0 0 0-.7 0L1.134 7.65a.495.495 0 0 0 0 .7l6.516 6.516a.495.495 0 0 0 .7 0l6.516-6.516a.495.495 0 0 0 0-.7L8.35 1.134Z"/>
</svg>
                </i>
                <h4>Flexible Strategies</h4>
                <p>
                  All the tools you need to manage all your projects for your company or products.
                </p>
              </div>
            </div>
            <div className="col-xl-4 col-lg-6 ">
              <div className="makes-us-different-text mb-0">
                <i>
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
  <path d="M1 2.828c.885-.37 2.154-.769 3.388-.893 1.33-.134 2.458.063 3.112.752v9.746c-.935-.53-2.12-.603-3.213-.493-1.18.12-2.37.461-3.287.811zm7.5-.141c.654-.689 1.782-.886 3.112-.752 1.234.124 2.503.523 3.388.893v9.923c-.918-.35-2.107-.692-3.287-.81-1.094-.111-2.278-.039-3.213.492zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783"/>
</svg>
                </i>
                <h4>Experience and knowledge</h4>
                <p>
                  Since 2003 we been working with clients all over the world, creating unique solutions.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="sercives gap">
        <div className="container">
          <div className="heading-boder two">
            <h2>
              How to achieve <br/>your goals?
            </h2>
            <p>Some of the methodologies we have for your projects.</p>
          </div>
          <div className="row">
            <div className="col-xl-3 col-lg-4 col-md-6">
              <div className="sercive-style">
                <img
                  alt="img"
                  className="w-100"
                  src="assets/img/agile.jpg"
                />
                <div className="sercive-style-text">
                  <h6>Agile</h6>
                  <div className="boder" />
                  <p>
                    The must flexible, reliable and clear methodology for digital projects.
                  </p>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-lg-4 col-md-6">
              <div className="sercive-style">
                <img
                  alt="img"
                  className="w-100"
                  src="assets/img/change-management.jpg"
                />
                <div className="sercive-style-text">
                  <h6>
                    Change management
                  </h6>
                  <div className="boder" />
                  <p>Reduce the impact and the resistance of new process.</p>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-lg-4 col-md-6">
              <div className="sercive-style">
                <img
                  alt="img"
                  className="w-100"
                  src="assets/img/certification.jpg"
                />
                <div className="sercive-style-text">
                  <h6>
                    Certifications
                  </h6>
                  <div className="boder" />
                  <p>
                    Create a champion for your company and spread the knowledge.
                  </p>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-lg-4 col-md-6">
              <div className="sercive-style mb-0">
                <img
                  alt="img"
                  className="w-100"
                  src="assets/img/training.jpg"
                />
                <div className="sercive-style-text">
                  <h6>
                    Training
                  </h6>
                  <div className="boder" />
                  <p>Unify the knowledge around your teams and company.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="gap map-three">
        <ContactForm />
      </section>
    </Layout>
  );
};
export default PM;

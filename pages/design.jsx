import ContactForm from "@/src/components/ContactForm";
import Layout from "@/src/layout/Layout";
const Design = () => {
  return (
    <Layout pageName={"Design"}>
      <section className="hero-section-three">
        <div
          className="swiper-container herothreeswiper"
        >
          <div className="swiper-wrapper">
              <div className="row align-items-center">
                <div className="col-xl-4 offset-xl-1">
                  <div className="heading-boder">
                    <h2>
                      Better design experience for your needs.
                    </h2>
                    <p>
                      where creativity meets strategy to shape your unique story.
                    </p>
                  </div>
                </div>
                <div className="col-xl-7">
                  <div className="three-page-img">
                    <img
                      alt="img"
                      className="w-100"
                      src="assets/img/design-hero.jpg"
                    />
                  </div>
                </div>
              </div>
          </div>
          <div className="swiper-pagination two-style" />
        </div>
      </section>
      <section
        className="makes-us-different"
        style={{ backgroundColor: "#f2edf5" }}
      >
        <div className="container">
          <div className="heading">
            <h6>How It Works?</h6>
            <h2>What Makes Us the option?</h2>
          </div>
          <div className="row">
            <div className="col-xl-4 col-lg-6 ">
              <div className="makes-us-different-text">
                <i>
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M6 3.5A1.5 1.5 0 0 1 7.5 2h1A1.5 1.5 0 0 1 10 3.5v1A1.5 1.5 0 0 1 8.5 6v1H14a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-1 0V8h-5v.5a.5.5 0 0 1-1 0V8h-5v.5a.5.5 0 0 1-1 0v-1A.5.5 0 0 1 2 7h5.5V6A1.5 1.5 0 0 1 6 4.5zM8.5 5a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5zM0 11.5A1.5 1.5 0 0 1 1.5 10h1A1.5 1.5 0 0 1 4 11.5v1A1.5 1.5 0 0 1 2.5 14h-1A1.5 1.5 0 0 1 0 12.5zm1.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5zm4.5.5A1.5 1.5 0 0 1 7.5 10h1a1.5 1.5 0 0 1 1.5 1.5v1A1.5 1.5 0 0 1 8.5 14h-1A1.5 1.5 0 0 1 6 12.5zm1.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5zm4.5.5a1.5 1.5 0 0 1 1.5-1.5h1a1.5 1.5 0 0 1 1.5 1.5v1a1.5 1.5 0 0 1-1.5 1.5h-1a1.5 1.5 0 0 1-1.5-1.5zm1.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5z"/>
</svg>
                </i>
                <h4>Functional solutions</h4>
                <p>
                  Creative solutions for your business needs focused on client's experience.
                </p>
              </div>
            </div>
            <div className="col-xl-4 col-lg-6 ">
              <div className="makes-us-different-text">
                <i>
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
  <path d="M5 8.5A2.5 2.5 0 0 1 7.5 6H9V4.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L9.41 8.658A.25.25 0 0 1 9 8.466V7H7.5A1.5 1.5 0 0 0 6 8.5V11H5z"/>
  <path fill-rule="evenodd" d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.48 1.48 0 0 1 0-2.098zm1.4.7a.495.495 0 0 0-.7 0L1.134 7.65a.495.495 0 0 0 0 .7l6.516 6.516a.495.495 0 0 0 .7 0l6.516-6.516a.495.495 0 0 0 0-.7L8.35 1.134Z"/>
</svg>
                </i>
                <h4>Flexible Strategies</h4>
                <p>
                  All the tools you need to create a professional designs for your company or products.
                </p>
              </div>
            </div>
            <div className="col-xl-4 col-lg-6 ">
              <div className="makes-us-different-text mb-0">
                <i>
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
  <path d="M1 2.828c.885-.37 2.154-.769 3.388-.893 1.33-.134 2.458.063 3.112.752v9.746c-.935-.53-2.12-.603-3.213-.493-1.18.12-2.37.461-3.287.811zm7.5-.141c.654-.689 1.782-.886 3.112-.752 1.234.124 2.503.523 3.388.893v9.923c-.918-.35-2.107-.692-3.287-.81-1.094-.111-2.278-.039-3.213.492zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783"/>
</svg>
                </i>
                <h4>Experience and knowledge</h4>
                <p>
                  Since 2003 we been working with clients all over the world, creating unique designs.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="satisfied-clients gap">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-xl-4">
              <div className="heading-boder">
                <h2>
                  Specialized Design
                </h2>
                <p className="pb-4">
                  Several areas of expertise to create the best design for your company.
                </p>
                <a href="contact" className="themebtu">
                  Ask us about them!
                </a>
              </div>
            </div>
            <div className="col-xl-4 col-lg-6">
              <div className="client-review">
                <i>
                  <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" viewBox="0 0 16 16">
  <path d="M0 2a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1zm8.5 0v8H15V2zm0 9v3H15v-3zm-1-9H1v3h6.5zM1 14h6.5V6H1z"/>
</svg>
                </i>
                <h4>User Interface Design</h4>
                <p>
                  The process of creating interfaces in software or computerized devices with a focus on user experience.
                </p>
              </div>
              <div className="client-review mt-xl-5">
                <i>
                  <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" viewBox="0 0 16 16">
  <path d="M15.825.12a.5.5 0 0 1 .132.584c-1.53 3.43-4.743 8.17-7.095 10.64a6.1 6.1 0 0 1-2.373 1.534c-.018.227-.06.538-.16.868-.201.659-.667 1.479-1.708 1.74a8.1 8.1 0 0 1-3.078.132 4 4 0 0 1-.562-.135 1.4 1.4 0 0 1-.466-.247.7.7 0 0 1-.204-.288.62.62 0 0 1 .004-.443c.095-.245.316-.38.461-.452.394-.197.625-.453.867-.826.095-.144.184-.297.287-.472l.117-.198c.151-.255.326-.54.546-.848.528-.739 1.201-.925 1.746-.896q.19.012.348.048c.062-.172.142-.38.238-.608.261-.619.658-1.419 1.187-2.069 2.176-2.67 6.18-6.206 9.117-8.104a.5.5 0 0 1 .596.04M4.705 11.912a1.2 1.2 0 0 0-.419-.1c-.246-.013-.573.05-.879.479-.197.275-.355.532-.5.777l-.105.177c-.106.181-.213.362-.32.528a3.4 3.4 0 0 1-.76.861c.69.112 1.736.111 2.657-.12.559-.139.843-.569.993-1.06a3 3 0 0 0 .126-.75zm1.44.026c.12-.04.277-.1.458-.183a5.1 5.1 0 0 0 1.535-1.1c1.9-1.996 4.412-5.57 6.052-8.631-2.59 1.927-5.566 4.66-7.302 6.792-.442.543-.795 1.243-1.042 1.826-.121.288-.214.54-.275.72v.001l.575.575zm-4.973 3.04.007-.005zm3.582-3.043.002.001h-.002z"/>
</svg>
                </i>
                <h4>Identity Design</h4>
                <p>
                  The visual representation of a brand, including its logo, color scheme, and typography, to create a distinct and recognizable image.
                </p>
              </div>
            </div>
            <div className="col-xl-4 col-lg-6">
              <div className="client-review mb-xl-5">
                <i>
                  <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" viewBox="0 0 16 16">
  <path d="M13.5 3a.5.5 0 0 1 .5.5V11H2V3.5a.5.5 0 0 1 .5-.5zm-11-1A1.5 1.5 0 0 0 1 3.5V12h14V3.5A1.5 1.5 0 0 0 13.5 2zM0 12.5h16a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 12.5"/>
</svg>
                </i>
                <h4>Web Design</h4>
                <p>
                  Web design involves creating the look and feel of websites, focusing on aesthetics, usability, and user experience.
                </p>
              </div>
              <div className="client-review">
                <i className="fa-solid fa-quote-left" />
                <h4>Socialmedia Design</h4>
                <p>
                  Social media design is about creating visually appealing content for platforms like Facebook, Instagram, and Twitter to engage and attract followers.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="gap map-three">
        <ContactForm />
      </section>
    </Layout>
  );
};
export default Design;

import ContactForm from "@/src/components/ContactForm";
import Layout from "@/src/layout/Layout";
const Services = () => {
  return (
    <Layout noHeaderBg pageName={"Services"}>
      <section
        className="splash-area-section"
        style={{ backgroundImage: "url(assets/img/background.jpg)" }}
      >
        <div className="container">
          <div className="splash-area">
            <h2>
              We Solve
              <br /> Business needs
            </h2>
            <a href="#">Coding, Implementing, Management</a>
          </div>
        </div>
      </section>
      <section
        className="discovery gap no-bottom"
        style={{ backgroundColor: "#f2edf5" }}
      >
        <div className="container">
          <div className="row">
            <div className="col-xl-6">
              <div className="heading">
                <h6>Project management, Digitalization and App development</h6>
                <h2 className="pt-3 pt-4">A Complete Service for your company needs.</h2>
                <p>
                  Maximize your brand's potential with our comprehensive design, 
                  printing, and web development services. 
                  From eye-catching visuals to seamless online experiences, we've got you covered
                </p>
              </div>
            </div>
            <div className="col-xl-6">
              <div className="discovery-img hoverstyle">
                <figure>
                  <img
                    className="w-100"
                    alt="discovery"
                    src="assets/img/services-intro.jpg"
                  />
                </figure>
                <div className="discovery-text">
                  <i>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
  <path d="M2 10h3a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1v-3a1 1 0 0 1 1-1m9-9h3a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-3a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1m0 9a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h3a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1zm0-10a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h3a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM2 9a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h3a2 2 0 0 0 2-2v-3a2 2 0 0 0-2-2zm7 2a2 2 0 0 1 2-2h3a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-3a2 2 0 0 1-2-2zM0 2a2 2 0 0 1 2-2h3a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2zm5.354.854a.5.5 0 1 0-.708-.708L3 3.793l-.646-.647a.5.5 0 1 0-.708.708l1 1a.5.5 0 0 0 .708 0z"/>
</svg>
                  </i>
                  <a href="#">
                    <h4>Multi services</h4>
                  </a>
                  <p>
                    Elevate and enhance your business with our exceptionally vast and comprehensive range of services. 
                    From captivating design to precision printing and seamless web development, we've got you covered!
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

      </section>
      <section className="sercives">
        <div className="container">
          <div className="heading-boder two">
            <h2>
              We’ll offer multiple solutions &amp; <span>services</span>
            </h2>
            <p>We are an always curious, strategic-creative digital agency</p>
          </div>
          <div className="row">
            <div className="col-lg-4 col-md-6">
              <div className="sercive-style">
                <img
                  alt="img"
                  className="w-100"
                  src="assets/img/services-list-1.jpg"
                />
                <div className="sercive-style-text">
                  <h6>Project management services</h6>
                  <div className="boder" />
                  <p>
                    Discover the power of well managed projects and complete all of them seamlessly.
                  </p>
                  <a href="project-management.html">More Info</a>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-6">
              <div className="sercive-style">
                <img
                  alt="img"
                  className="w-100"
                  src="assets/img/services-list-2.jpg"
                />
                <div className="sercive-style-text">
                  <h6>
                    Process digitalization
                  </h6>
                  <div className="boder" />
                  <p>
                    Transform your company implementing the lates technology to make your delivery faster than ever.
                  </p>
                  <a href="process-digitalization.html">More Info</a>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-md-6">
              <div className="sercive-style mb-0">
                <img
                  alt="img"
                  className="w-100"
                  src="assets/img/services-list-3.jpg"
                />
                <div className="sercive-style-text">
                  <h6>
                    Web development
                  </h6>
                  <div className="boder" />
                  <p>
                    Take your online presence to the next level! Check our Web Development Services section for innovative solutions tailored to your business.
                  </p>
                  <a href="services.html">Read More</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="gap map-three">
        <ContactForm />
      </section>
    </Layout>
  );
};
export default Services;

import { Head, Html, Main, NextScript } from 'next/document';

export default function Document() {
    return (
        <Html lang="en">
            <Head>
                <link rel="icon" href="assets/img/logo-icon.png" />
                {/* CSS only */}
                <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" />
                {/* Font Awesome 6 */}
                <link rel="stylesheet" href="assets/css/fontawesome.min.css" />
                <link rel="stylesheet" href="assets/css/magnific-popup.min.css" />
                <link rel="stylesheet" href="assets/css/swiper.css" />
                {/* style */}
                <link rel="stylesheet" href="assets/css/style.css" />
                {/* responsive */}
                <link rel="stylesheet" href="assets/css/responsive.css" />
                {/* color */}
                <link rel="stylesheet" href="assets/css/color.css" />
            </Head>
            <body>
                <Main />
                <NextScript />
                <script src="https://web3forms.com/client/script.js" async defer></script>
            </body>
        </Html>
    );
}

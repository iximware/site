import Layout from "@/src/layout/Layout";
const Index = () => {
  return (
    <Layout noHeaderBg pageName={"Home"}>
      <section
        className="hero-section-one"
        style={{ backgroundImage: "url(assets/img/hero-img.jpg)" }}
      >
        <div className="container">
          <div className="heading-boder">
            <h2>
              <span>design</span>
              the identity of your business
            </h2>
          </div>
        </div>
      </section>
      <div>
        <div className="container">
          <div className="small-services-color">
            <div className="row">
              <div className="col-xl-4 col-lg-6">
                <div className="small-services">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
  <path d="M8 5a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3m4 3a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3M5.5 7a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0m.5 6a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3"/>
  <path d="M16 8c0 3.15-1.866 2.585-3.567 2.07C11.42 9.763 10.465 9.473 10 10c-.603.683-.475 1.819-.351 2.92C9.826 14.495 9.996 16 8 16a8 8 0 1 1 8-8m-8 7c.611 0 .654-.171.655-.176.078-.146.124-.464.07-1.119-.014-.168-.037-.37-.061-.591-.052-.464-.112-1.005-.118-1.462-.01-.707.083-1.61.704-2.314.369-.417.845-.578 1.272-.618.404-.038.812.026 1.16.104.343.077.702.186 1.025.284l.028.008c.346.105.658.199.953.266.653.148.904.083.991.024C14.717 9.38 15 9.161 15 8a7 7 0 1 0-7 7"/>
</svg>
                  <div>
                    <a href="services.html">Design services</a>
                    <span>We help you to get the swag!</span>
                  </div>
                </div>
              </div>
              <div className="col-xl-4 col-lg-6">
                <div className="small-services">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
  <path d="M2.5 8a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1"/>
  <path d="M5 1a2 2 0 0 0-2 2v2H2a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h1v1a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-1h1a2 2 0 0 0 2-2V7a2 2 0 0 0-2-2h-1V3a2 2 0 0 0-2-2zM4 3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1v2H4zm1 5a2 2 0 0 0-2 2v1H2a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-1v-1a2 2 0 0 0-2-2zm7 2v3a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1"/>
</svg>
                  <div>
                    <a href="services.html">Print</a>
                    <span>Make your ideas alive!</span>
                  </div>
                </div>
              </div>
              <div className="col-xl-4 col-lg-6">
                <div className="small-services lest mb-0">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
  <path d="M2.5 4a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1m2-.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0m1 .5a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1"/>
  <path d="M2 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2zm12 1a1 1 0 0 1 1 1v2H1V3a1 1 0 0 1 1-1zM1 13V6h4v8H2a1 1 0 0 1-1-1m5 1V6h9v7a1 1 0 0 1-1 1z"/>
</svg>
                  <div>
                    <a href="services.html">Web apps</a>
                    <span>Code and deploy your business!</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="gap no-bottom">
        <img className="w-100 pb-5 mb-5" alt="line" src="assets/img/line.jpg" />
        <div className="container">
          <div className="row">
            <div className="col-xl-6">
              <div className="welcome">
                <img alt="img" src="assets/img/brain-bulb.jpg" />
              </div>
            </div>
            <div className="col-xl-6 pl-75">
              <div className="heading design-enjoy">
                <h6>Get your ideas alive!</h6>
                <h2> We're a Design Agency.</h2>
              </div>
              <div className="welcome-text">
                <h6 className="pt-4 pb-5">
                  Identity is the key. Let's make it unique!
                </h6>
                <h5>Graphic Design</h5>
                <p>
                  At <strong><i>iximware</i></strong>, we specialize in crafting stunning graphic designs tailored just for you. 
                  Whether you need a striking logo, eye-catching marketing materials, or engaging website graphics.{" "}
                </p>
                <h5>Digital Design</h5>
                <p>
                  we craft designs that resonate with your audience and amplify your brand's message. 
                  Let's craft a stunning digital presence that sets you apart from the crowd.
                </p>
                <a href="design" className="themebtu">
                  More about design...
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="gap services-two no-bottom">
        <div className="container">
          <div className="row">
            <div className="col-xl-5">
              <div className="heading">
                <h6>Innovation in technology</h6>
                <h2>
                  {" "}
                  Web Development services for your next project.
                </h2>
              </div>
              <div className="welcome-text">
                <h6 className="pt-4 pb-4">
                  From simple websites to complex web applications, we've got you covered.
                </h6>
                <h5>Focus on User Experience</h5>
                <p>
                  We understand the importance of delivering an exceptional user experience. 
                  Our experience crafting bespoke websites designed to engage and delight your audience. {" "}
                </p>
                <h5>Functionality and design united</h5>
                <p>
                  It means ensuring that your website, application,
                  or any digital product not only looks visually stunning but also functions seamlessly to meet your goals.
                </p>
                <a href="web-development" className="themebtu">
                  More about web development...
                </a>
              </div>
            </div>
            <div className="offset-xl-1 col-xl-6">
              <div className="welcome two">
                <img alt="img" src="assets/img/web-development.jpg" />
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="gap no-bottom">
        <div className="container">
          <div className="row">
            <div className="col-xl-6">
              <div className="welcome">
                <img alt="img" src="assets/img/printing-process.jpg" />
              </div>
            </div>
            <div className="col-xl-6 pl-75">
              <div className="heading design-enjoy">
                <h6>From digital ro real live.</h6>
                <h2>Printing services with many techniques.</h2>
              </div>
              <div className="welcome-text">
                <h6 className="pt-4 pb-5">
                  Providing top-quality print services tailored to your needs.
                  Your satisfaction is our priority. Contact us for expert assistance.
                </h6>
                <h5>Different techniques, better results</h5>
                <p>
                  Unlock versatility and creativity with multiple printing techniques,
                  ensuring your designs stand out and make a lasting impact.{" "}
                </p>
                <h5>The best option for your needs</h5>
                <p>
                  We analyze your needs and recommend the best printing technique for your project. Save time and money with our expert advice.
                </p>
                <a href="services.html" className="themebtu">
                  More about printing...
                </a>
              </div>
            </div>
          </div>
        </div>
        <img className="w-100" alt="line" src="assets/img/line.jpg" />
      </section>
      <section className="how-it-works gap no-top">
        <div className="container">
          <div className="heading">
            <h6>Why iximware?</h6>
            <h2> We Are Strategic, Managed and Experienced.</h2>
          </div>
          <div className="row pt-4">
            <div className="col-xl-4 col-lg-6">
              <div className="strategic">
                <h2>O1/</h2>
                <div className="pl-80">
                  <h4>Agile projects</h4>
                  <p> Offer flexibility, faster delivery, adaptability to changes, constant feedback, and collaboration, resulting in better quality and customer satisfaction.</p>
                </div>
              </div>
            </div>
            <div className="col-xl-4 col-lg-6">
              <div className="strategic">
                <h2>O2/</h2>
                <div className="pl-80">
                  <h4>Focus on final client</h4>
                  <p>
                    Client-focused projects prioritize customer needs, fostering loyalty, satisfaction, and repeat business, essential for small businesses' expansion and success.{" "}
                  </p>
                </div>
              </div>
            </div>
            <div className="col-xl-4 col-lg-6">
              <div className="strategic">
                <h2>O3/</h2>
                <div className="pl-80">
                  <h4>Innovation on solutions</h4>
                  <p>
                    Unlocks creativity, enhances user experience, differentiates brands, and drives growth by solving problems in unique and impactful ways.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="latest-blog gap">
        <div className="container">
          <div className="heading">
            <h6>check our latest activities</h6>
            <h2>Events</h2>
          </div>
          <div className="row">
            <div className="col-xl-4 col-md-6">
                <div className="latest-blog-post hoverstyle">
                  <figure>
                    <img
                      alt="img"
                      className="w-100"
                      src="assets/img/fuckup-nite-tb.jpg"
                    />
                  </figure>
                  <a href="#">
                    <i className="fa-regular fa-clock" />
                    <span>April, 2024</span>
                  </a>
                  <a href="fuckup-nite">
                    <h4>Fuckup nite!</h4>
                  </a>
                  <span>by iximware</span>
                </div>
              </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};
export default Index;

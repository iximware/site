import Layout from "@/src/layout/Layout";
const Index = () => {
  return (
    <Layout noHeaderBg pageName={"Home"}>
      <section
        className="hero-section-one"
        style={{ backgroundImage: "url(assets/img/hero-img.jpg)" }}
      >
        <div className="container">
          <div className="heading-boder">
            <h2>
              <span>lead and achieve</span>
              your projects
            </h2>
          </div>
        </div>
      </section>
      <div>
        <div className="container">
          <div className="small-services-color">
            <div className="row">
              <div className="col-xl-4 col-lg-6">
                <div className="small-services">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-card-checklist" viewBox="0 0 16 16">
  <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2z"/>
  <path d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5m-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0M7 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5m-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0"/>
</svg>
                  <div>
                    <a href="services.html">Project management</a>
                    <span>We help you to get the everything in order!</span>
                  </div>
                </div>
              </div>
              <div className="col-xl-4 col-lg-6">
                <div className="small-services">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-upc-scan" viewBox="0 0 16 16">
  <path d="M1.5 1a.5.5 0 0 0-.5.5v3a.5.5 0 0 1-1 0v-3A1.5 1.5 0 0 1 1.5 0h3a.5.5 0 0 1 0 1zM11 .5a.5.5 0 0 1 .5-.5h3A1.5 1.5 0 0 1 16 1.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 1-.5-.5M.5 11a.5.5 0 0 1 .5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 1 0 1h-3A1.5 1.5 0 0 1 0 14.5v-3a.5.5 0 0 1 .5-.5m15 0a.5.5 0 0 1 .5.5v3a1.5 1.5 0 0 1-1.5 1.5h-3a.5.5 0 0 1 0-1h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 1 .5-.5M3 4.5a.5.5 0 0 1 1 0v7a.5.5 0 0 1-1 0zm2 0a.5.5 0 0 1 1 0v7a.5.5 0 0 1-1 0zm2 0a.5.5 0 0 1 1 0v7a.5.5 0 0 1-1 0zm2 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3 0a.5.5 0 0 1 1 0v7a.5.5 0 0 1-1 0z"/>
</svg>
                  <div>
                    <a href="services.html">Process digitalization</a>
                    <span>Digitalize your process and save time.</span>
                  </div>
                </div>
              </div>
              <div className="col-xl-4 col-lg-6">
                <div className="small-services lest mb-0">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
  <path d="M2.5 4a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1m2-.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0m1 .5a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1"/>
  <path d="M2 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2zm12 1a1 1 0 0 1 1 1v2H1V3a1 1 0 0 1 1-1zM1 13V6h4v8H2a1 1 0 0 1-1-1m5 1V6h9v7a1 1 0 0 1-1 1z"/>
</svg>
                  <div>
                    <a href="services.html">Web apps</a>
                    <span>Code and deploy your business!</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="gap no-bottom">
        <img className="w-100 pb-5 mb-5" alt="line" src="assets/img/line.jpg" />
        <div className="container">
          <div className="row">
            <div className="col-xl-6">
              <div className="welcome">
                <img alt="img" src="assets/img/project-management.jpg" />
              </div>
            </div>
            <div className="col-xl-6 pl-75">
              <div className="heading design-enjoy">
                <h6>Complete your projects seamlessly!</h6>
                <h2> We're manage your projects.</h2>
              </div>
              <div className="welcome-text">
                <h6 className="pt-4 pb-5">
                  The key of success for every project is the smart management.
                </h6>
                <h5>Implement the right methodology</h5>
                <p>
                  At <strong><i>iximware</i></strong>, we specialize in Agile methodology, take the control of your project and deliver successfully on time.{" "}
                </p>
                <h5>Train your team</h5>
                <p>
                  Knowledge is power! make every team member an expert to follow the rules of the project and minimize the risks.
                </p>
                <a href="project-management" className="themebtu">
                  More about project management...
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="gap services-two no-bottom">
        <div className="container">
          <div className="row">
            <div className="col-xl-5">
              <div className="heading">
                <h6>Innovation in technology</h6>
                <h2>
                  {" "}
                  Web Development services for your next project.
                </h2>
              </div>
              <div className="welcome-text">
                <h6 className="pt-4 pb-4">
                  From simple websites to complex web applications, we've got you covered.
                </h6>
                <h5>Focus on User Experience</h5>
                <p>
                  We understand the importance of delivering an exceptional user experience. 
                  Our experience crafting bespoke websites designed to engage and delight your audience. {" "}
                </p>
                <h5>Functionality and design united</h5>
                <p>
                  It means ensuring that your website, application,
                  or any digital product not only looks visually stunning but also functions seamlessly to meet your goals.
                </p>
                <a href="web-development" className="themebtu">
                  More about web development...
                </a>
              </div>
            </div>
            <div className="offset-xl-1 col-xl-6">
              <div className="welcome two">
                <img alt="img" src="assets/img/web-development.jpg" />
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="gap no-bottom">
        <div className="container">
          <div className="row">
            <div className="col-xl-6">
              <div className="welcome">
                <img alt="img" src="assets/img/automation-process.jpg" />
              </div>
            </div>
            <div className="col-xl-6 pl-75">
              <div className="heading design-enjoy">
                <h6>Jump from analogy to digital world</h6>
                <h2>Accelerate your work digitalizing your processes</h2>
              </div>
              <div className="welcome-text">
                <h6 className="pt-4 pb-5">
                  Save time and resources transforming your traditional processes into a digital and automatic paths, manage your must valuable resource which is time.
                </h6>
                <h5>Automation and digitalization</h5>
                <p>
                  Ensure the fastest response in each process in your company moving from analogic processes to automatic actions saving time in all your common task.{" "}
                </p>
                <h5>Implement Artificial Intelligence</h5>
                <p>
                  Use the technology for your benefits and take advantage of the artificial intelligence simplifying tedious task, save time and spread efficiency through your teams. 
                </p>
                <a href="process-digitalization" className="themebtu">
                  More about automation...
                </a>
              </div>
            </div>
          </div>
        </div>
        <img className="w-100" alt="line" src="assets/img/line.jpg" />
      </section>
      <section className="how-it-works gap no-top">
        <div className="container">
          <div className="heading">
            <h6>Why iximware?</h6>
            <h2> We Are Strategic, Managed and Experienced.</h2>
          </div>
          <div className="row pt-4">
            <div className="col-xl-4 col-lg-6">
              <div className="strategic">
                <h2>O1/</h2>
                <div className="pl-80">
                  <h4>Agile projects</h4>
                  <p> Offer flexibility, faster delivery, adaptability to changes, constant feedback, and collaboration, resulting in better quality and customer satisfaction.</p>
                </div>
              </div>
            </div>
            <div className="col-xl-4 col-lg-6">
              <div className="strategic">
                <h2>O2/</h2>
                <div className="pl-80">
                  <h4>Focus on final client</h4>
                  <p>
                    Client-focused projects prioritize customer needs, fostering loyalty, satisfaction, and repeat business, essential for small businesses' expansion and success.{" "}
                  </p>
                </div>
              </div>
            </div>
            <div className="col-xl-4 col-lg-6">
              <div className="strategic">
                <h2>O3/</h2>
                <div className="pl-80">
                  <h4>Innovation on solutions</h4>
                  <p>
                    Unlocks creativity, enhances user experience, differentiates brands, and drives growth by solving problems in unique and impactful ways.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="latest-blog gap">
        <div className="container">
          <div className="heading">
            <h6>check our latest activities</h6>
            <h2>Events</h2>
          </div>
          <div className="row">
            <div className="col-xl-4 col-md-6">
                <div className="latest-blog-post hoverstyle">
                  <figure>
                    <img
                      alt="img"
                      className="w-100"
                      src="assets/img/fuckup-nite-tb.jpg"
                    />
                  </figure>
                  <a href="#">
                    <i className="fa-regular fa-clock" />
                    <span>April, 2024</span>
                  </a>
                  <a href="fuckup-nite">
                    <h4>Fuckup nite!</h4>
                  </a>
                  <span>by iximware</span>
                </div>
              </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};
export default Index;

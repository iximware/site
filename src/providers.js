'use client';
import posthog from 'posthog-js';
import { PostHogProvider } from 'posthog-js/react';

if (typeof window !== 'undefined') {
    posthog.init(process.env.NEXT_PUBLIC_PHK, {
        api_host: process.env.NEXT_PUBLIC_PHH,
    });
}

export function PHProvider({ children }) {
    return <PostHogProvider client={posthog}>{children}</PostHogProvider>;
}

import Link from 'next/link';
import { useState } from 'react';
import MobileHeader from './MobileHeader';
const Header = ({ headerExtraClass, noHeaderBg, blackLogo }) => {
    const [searchToggle, setSearchToggle] = useState(false);
    const [toggle, setToggle] = useState(false);
    const logoImg = blackLogo ? 'assets/img/logo-dark.svg' : 'assets/img/logo-light.svg';
    return (
        <header
            className={headerExtraClass ? headerExtraClass : ''}
            style={!noHeaderBg ? { backgroundImage: `url(assets/img/header.jpg)` } : { background: 'transparent' }}
        >
            <div className="container">
                <div className="nav">
                    <div className="d-flex align-items-center">
                        <div className="logo">
                            <Link legacyBehavior href="/">
                                <a>
                                    <img alt="logo" src={logoImg} height={24} />
                                </a>
                            </Link>
                        </div>
                        <ul className="menu">
                            <li>
                                <Link legacyBehavior href="/">
                                    Home
                                </Link>
                            </li>
                            <li>
                                <Link legacyBehavior href="services">
                                    Services
                                </Link>
                                <ul className="sub-menu">
                                    <li>
                                        <Link legacyBehavior href="project-management">
                                            Project management
                                        </Link>
                                    </li>
                                    <li>
                                        <Link legacyBehavior href="process-digitalization">
                                            Process digitalization
                                        </Link>
                                    </li>
                                    <li>
                                        <Link legacyBehavior href="web-development">
                                            Web Development
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <Link legacyBehavior href="events">
                                    Events
                                </Link>
                                <ul className="sub-menu">
                                    <li>
                                        <Link legacyBehavior href="fuckup-nite">
                                            Fuckup nite!
                                        </Link>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <Link legacyBehavior href="contact">
                                    Contact
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <Link legacyBehavior href="/contact">
                            <a className="themebtu full">Get Started</a>
                        </Link>
                    </div>
                    <div className="bar-menu" onClick={() => setToggle(true)}>
                        <i className="fa-solid fa-bars" />
                    </div>
                </div>
            </div>
            <MobileHeader toggle={toggle} close={() => setToggle(false)} />
        </header>
    );
};
export default Header;

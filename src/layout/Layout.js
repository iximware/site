import Head from 'next/head';
import { Fragment } from 'react';
import ImageView from '../components/ImageView';
import VideoPopup from '../components/VideoPopup';
import { PHProvider } from '../providers';
import Footer from './Footer';
import Header from './Header';
import ScrollTop from './ScrollTop';
const Layout = ({ children, headerExtraClass, noHeaderBg, blackLogo, pageName, pageDescription }) => {
    const title = pageName || 'iximware';
    const description = pageDescription || 'fostering innovation and creativity in the digital world.';
    return (
        <Fragment>
            <Head>
                <title>{title}</title>
                <meta name="description" content={description} />
            </Head>
            <PHProvider>
                <VideoPopup />
                <ImageView />
                <Header headerExtraClass={headerExtraClass} noHeaderBg={noHeaderBg} blackLogo={blackLogo} />
                {children}
                <Footer />
                <ScrollTop />
            </PHProvider>
        </Fragment>
    );
};
export default Layout;

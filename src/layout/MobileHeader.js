import Link from 'next/link';
import { useState } from 'react';
const MobileHeader = ({ toggle, close }) => {
    const [activeMenu, setActiveMenu] = useState(null);
    const active = value => setActiveMenu(value === activeMenu ? null : value),
        activeSubMenu = value => (value == activeMenu ? 'active' : '');
    return (
        <div className={`mobile-nav ${toggle ? 'open' : 'hmburger-menu'}`} id="mobile-nav" style={{ display: 'block' }}>
            <div className="res-log">
                <a href="index.html">
                    <img src="assets/img/logo-light.svg" alt="Responsive Logo" height={24} />
                </a>
            </div>
            <ul>
                <li className={`${activeSubMenu('home')}`}>
                    <Link legacyBehavior href="/">
                        Home
                    </Link>
                </li>
                <li className={`menu-item-has-children ${activeSubMenu('Services')}`}>
                    <a href="JavaScript:void(0)" onClick={() => active('Services')}>
                        Services
                    </a>
                    <ul className="sub-menu">
                        <li>
                            <Link legacyBehavior href="design">
                                Design
                            </Link>
                        </li>
                        <li>
                            <Link legacyBehavior href="print">
                                Print
                            </Link>
                        </li>
                        <li>
                            <Link legacyBehavior href="web-development">
                                Web Development
                            </Link>
                        </li>
                    </ul>
                </li>
                <li className={`menu-item-has-children ${activeSubMenu('Events')}`}>
                    <a href="JavaScript:void(0)" onClick={() => active('Events')}>
                        Events
                    </a>
                    <ul className="sub-menu">
                        <li>
                            <Link legacyBehavior href="fuckup-nite">
                                Fuckup Nite!
                            </Link>
                        </li>
                    </ul>
                </li>
                <li>
                    <Link legacyBehavior href="contact">
                        Contact
                    </Link>
                </li>
            </ul>
            <a href="JavaScript:void(0)" id="res-cross" onClick={() => close()} />
        </div>
    );
};
export default MobileHeader;

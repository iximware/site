const Footer = () => {
    return (
        <footer className="gap no-bottom" style={{ backgroundColor: '#222' }}>
            <div className="container">
                <div className="row">
                    <div className="col-xl-3 col-lg-4 col-6">
                        <div className="footer-logo">
                            <a href="index.html">
                                <img alt="img" src="assets/img/logo-light.svg" />
                            </a>
                            <p>foster innovation</p>
                        </div>
                    </div>
                    <div className="col-xl-3 offset-xl-6 col-lg-4 offset-lg-4 col-6">
                        <div className="links">
                            <ul>
                                <li>
                                    <a href="/">Home</a>
                                </li>
                                <li>
                                    <a href="services">Services</a>
                                </li>
                                <li>
                                    <a href="events">Events</a>
                                </li>
                                <li className="pb-0">
                                    <a href="contact">Contact us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="footer-bottom">
                    <p>© 2024 iximware</p>
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/iximwaremx">
                                <i className="fa-brands fa-facebook-f" />
                            </a>
                        </li>
                        <li>
                            <a href="https://www.x.com/iximwaremx">
                                <i className="fa-brands fa-twitter" />
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/iximwaremx/">
                                <i className="fa-brands fa-instagram" />
                            </a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/iximwaremx">
                                <i className="fa-brands fa-linkedin-in" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </footer>
    );
};
export default Footer;

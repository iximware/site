const ContactForm = () => {
    return (
        <div className="container">
            <div className="row">
                <div className="col-xl-6">
                    <div className="heading">
                        <h6>We hear you!</h6>
                        <h2>Let’s collaborate</h2>
                        <p>
                            Got questions or ideas? We'd love to hear from you! Drop us a message and let's start the
                            conversation.{' '}
                        </p>
                    </div>
                    <form className="touch two" action="https://api.web3forms.com/submit" method="POST">
                        <div className="row">
                            <div className="col-lg-4">
                                <input type="text" name="name" placeholder="Your name *" required />
                            </div>
                            <div className="col-lg-4">
                                <input type="text" name="email" placeholder="Email address *" required />
                            </div>
                            <div className="col-lg-4">
                                <input type="number" name="phone" placeholder="Your Phone (optional)" />
                            </div>
                            <div className="col-xl-12">
                                <textarea placeholder="Your message *" defaultValue={''} required />
                            </div>
                            <div className="btugap">
                                <a href="#" className="themebtu full">
                                    Send Message
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
                <div className="offset-xl-1 col-xl-5">
                    <div className="map">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d318.2524357339499!2d-105.20704165576184!3d20.7143080953639!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842148bcbe11c565%3A0x938917e568bccb91!2sJu%C3%A1rez%20179%2C%20Centro%20Ixtapa%2C%2048280%20Ixtapa%2C%20Jal.!5e0!3m2!1sen!2smx!4v1711776926798!5m2!1sen!2smx"
                            style={{ border: 0 }}
                            allowFullScreen
                            loading="lazy"
                            referrerPolicy="no-referrer-when-downgrade"
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ContactForm;
